﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    //Config
    [SerializeField] float minX = 1f;
    [SerializeField] float maxX = 15f;
    [SerializeField] float screenWidthUnits = 16f;

    [SerializeField] Rigidbody2D paddleBody;

    // Start is called before the first frame update
    void Start()
    {
        paddleBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float mousePos = (Input.mousePosition.x / Screen.width * screenWidthUnits);
        Vector2 paddlePos = new Vector2(paddleBody.position.x, 0.25f);
        paddlePos.x = Mathf.Clamp(mousePos, minX, maxX);
        paddleBody.MovePosition(paddlePos);
    }

    public Vector2 GetPosition()
    {
        return paddleBody.position;
    }
}
