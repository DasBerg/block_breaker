﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] int pointsPerDamage = 10;
    [SerializeField] int pointsPerBreak = 50;
    [SerializeField] Sprite damaged1;
    [SerializeField] Sprite damaged2;

    [SerializeField] AudioClip destroySound;

    int hits = 0;

    Level level;
    GameStatus gameStatus;

    private void Start()
    {
        level = FindObjectOfType<Level>();
        gameStatus = FindObjectOfType<GameStatus>();

        level.CountBreakableBlocks();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (hits == 0)
        {
            GetComponent<SpriteRenderer>().sprite = damaged1;
            gameStatus.AddPoints(pointsPerDamage);
        }
        else if (hits == 1)
        {
            GetComponent<SpriteRenderer>().sprite = damaged2;
            gameStatus.AddPoints(pointsPerDamage);
        }
        else
        {
            AudioSource.PlayClipAtPoint(destroySound, transform.position);
            level.RemoveBreakableBlock();
            gameStatus.AddPoints(pointsPerBreak);
            Destroy(gameObject);
        }

        hits++;
    }
}
