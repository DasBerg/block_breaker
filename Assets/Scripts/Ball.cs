﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] Rigidbody2D ballBody;
    [SerializeField] Paddle paddle;
    [SerializeField] float launchVelocityX = 15f;
    [SerializeField] float launchVelocityY = 2f;

    Vector2 paddleToBallVector;
    bool hasStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        ballBody = GetComponent<Rigidbody2D>();
        paddleToBallVector = ballBody.position - paddle.GetPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasStarted)
        {
            LockBall();
            LaunchBall();
        }
    }

    void LockBall()
    {
        Vector2 paddlePos = paddle.GetPosition();
        ballBody.position = paddlePos + paddleToBallVector;
    }

    void LaunchBall()
    {
        if (Input.GetMouseButtonDown(0))
        {
            hasStarted = true;
            ballBody.velocity = new Vector2(launchVelocityY, launchVelocityX);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (hasStarted)
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
